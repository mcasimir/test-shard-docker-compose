const childProcess = require('child_process');
const commandExists = require('command-exists');
const path = require('path');
const waitPort = require('wait-port');

module.exports = class DockerCompose {
  static async isAvailable() {
    return await commandExists('docker-compose');
  }

  constructor(projectName, dockerComposeFile, {otherArguments = [], otherFiles = [], cwd = '' } = {}) {
    this._cwd = cwd = path.dirname(path.resolve(process.cwd(), dockerComposeFile));

    this._defaultArguments = [
      '-p', projectName,
      '-f', dockerComposeFile,
      ...otherArguments
    ];

    for (const file of otherFiles) {
      this._defaultArguments.push('-f', file);
    }
  }

  getServicePort(serviceName, originalPort) {
    const portString = this.getServiceAddress(serviceName, originalPort).split(':')[1];
    return parseInt(portString);
  }

  getServiceAddress(serviceName, originalPort) {
    return this._runDockerCompose(['port', serviceName, `${originalPort}`], {
      stdio: 'pipe'
    });
  }

  _runDockerCompose(args, options) {
    const allArgs = [...this._defaultArguments, '--', ...args]
    const allOptions = {
      stdio: 'inherit',
      cwd: this._cwd,
      ...options
    };

    const result = childProcess
      .execFileSync(
        'docker-compose', allArgs, allOptions
      );

    return (result || '').toString().trim();
  }

  async awaitServicePort(serviceName, originalPort) {
    const [
      hostname, port
    ] = this.getServiceAddress(serviceName, originalPort).split(':');

    await waitPort({ host: hostname, port: parseInt(port) });
  }

  async up() {
    this._runDockerCompose(['up', '-d']);
  }

  async down() {
    this._runDockerCompose(['down']);
  }
}

