const { MongoClient } = require('mongodb');
const DockerCompose = require('./helpers/docker-compose');

describe('sharded cluster', () => {
  const shardedCluster = new DockerCompose(
    'mongosh-test-sharded-cluster',
    './docker-compose-sharded.yml'
  );

  before(async function() {
    if (!await DockerCompose.isAvailable()) {
      this.skip();
    }

    shardedCluster.up();
    await shardedCluster.awaitServicePort('mongodb-sharded', 27017);
  });

  after(() => {
    shardedCluster.down();
  });


  it('starts and can connect', async() => {
    const port = shardedCluster.getServicePort(
      'mongodb-sharded', 27017
    );

    const client = new MongoClient(
      `mongodb://localhost:${port}/`,
      {
        useUnifiedTopology: true,
        useNewUrlParser: true
      }
    );

    await client.connect();
    await client.close();
  });
});
